#include <termios.h>
#include <stdio.h>
#include <unistd.h>
#include <fcntl.h>
#include <sys/signal.h>
#include <sys/types.h>
#include <stdio.h>   /* Standard input/output definitions */
#include <string.h>  /* String function definitions */
#include <unistd.h>  /* UNIX standard function definitions */
#include <fcntl.h>   /* File control definitions */
#include <errno.h>   /* Error number definitions */
#include <termios.h> /* POSIX terminal control definitions */
#include <sys/termios.h> /* POSIX terminal control definitions */
#include <sys/socket.h> /* POSIX terminal control definitions */
#include <netdb.h> 
#include <stdlib.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <mysql/mysql.h>

#include <sys/ioctl.h>

#define EXIT_SUCCESS 0
#define EXIT_FAILURE 1

//#define ADDRESS "192.168.1.137"
//#define ADDRESS "194.85.80.57"
char *ADDRESS;
char *USER; 
char *PASSWORD; 
char *BASE;

char *STIRALKA_ADDRESS;
char *STIRALKA_USER; 
char *STIRALKA_PASSWORD; 
char *STIRALKA_BASE;

void read_config(char *filename){
    ADDRESS = (char *)malloc(256*sizeof(char));
    USER = (char *)malloc(256*sizeof(char));
    PASSWORD = (char *)malloc(256*sizeof(char));
    BASE = (char *)malloc(256*sizeof(char));
    FILE* f = fopen(filename,"r");
    fscanf(f,"%s",ADDRESS);
    fscanf(f,"%s",USER);
    fscanf(f,"%s",PASSWORD);
    fscanf(f,"%s",BASE);
    fclose(f);
}

void read_stiralka_config(char *filename){
    STIRALKA_ADDRESS = (char *)malloc(256*sizeof(char));
    STIRALKA_USER = (char *)malloc(256*sizeof(char));
    STIRALKA_PASSWORD = (char *)malloc(256*sizeof(char));
    STIRALKA_BASE = (char *)malloc(256*sizeof(char));
    FILE* f = fopen(filename,"r");
    fscanf(f,"%s",STIRALKA_ADDRESS);
    fscanf(f,"%s",STIRALKA_USER);
    fscanf(f,"%s",STIRALKA_PASSWORD);
    fscanf(f,"%s",STIRALKA_BASE);
    fclose(f);
}


int stiralka_time_access_request(char *number) { 
	puts("SQL connection init...");
	MYSQL *stiralka_mysql = mysql_init(NULL);
	MYSQL *mysql = mysql_init(NULL);
	if (mysql == NULL) {
		puts("[ERROR] in mysql_init");
		return 0;
	}
	else if (stiralka_mysql == NULL) {
		puts("[ERROR] in stiralka_mysql_init");
		return 0;
    }
    puts("Connecting to sql server...");
   	mysql = mysql_real_connect(mysql, ADDRESS, USER, PASSWORD, BASE, 0, NULL, 0);
	stiralka_mysql = mysql_real_connect(stiralka_mysql, STIRALKA_ADDRESS, STIRALKA_USER, STIRALKA_PASSWORD, STIRALKA_BASE, 0, NULL, 0);
	if (mysql == NULL) {
		puts("[ERROR] in mysql_real_connect");
		return 0;
	}
	else if (stiralka_mysql == NULL) {
		puts("[ERROR] in stiralka_mysql_real_connect");
		return 0;
    }
	
    char buf[500];
    *buf = '\0';
    sprintf(buf, "SELECT id, full_laundry_access FROM users WHERE trim(card)='Mifare%s'",number);
	
    puts("Try to get all users' ID from card's Mifare...");
    puts(buf);
	
    int error = mysql_query(mysql, buf);
    if(error){
		puts("[ERROR] in mysql_query");
		return 0;
    }
    puts("mysql_store_result...");
	
    MYSQL_RES *res = mysql_store_result(mysql);
    if(res == NULL){
		puts("[ERROR] in mysql_store_result");
		return 0;
    }
	puts("\n Get access to laundry");
    MYSQL_ROW row;

    while (row = mysql_fetch_row(res)) {
		puts("\n <First user check>");
		//первая проверка, на интервал в +-30 минут
		*buf = '\0';
		sprintf(buf, "SELECT * FROM op_signups, op_users WHERE (unix_timestamp(now()) < s_stop*60 + 1800) AND (unix_timestamp(now()) > s_start*60 - 1800) AND (s_uid = u_id) AND (u_keyid = %s)",row[0]);
		puts(buf);
		
		//xX знакомтесь - Его В-Во, Костыль-Ужасный Хх //
		//проверка флага full_laundry_access == 1
		char tmp[10];
		sprintf(tmp, "%s",row[1]);
		if (tmp[0] == '1') {
			mysql_free_result(res);
			puts("stiralka_mysql_close...");
			mysql_close(stiralka_mysql);
			mysql_close(mysql);
			return 1;
		}
		
		error = mysql_query(stiralka_mysql, buf);
		if(error){
			puts("[ERROR] in stiralka_mysql_query");
		}
		MYSQL_RES *stiralka_res = mysql_store_result(stiralka_mysql);
		
		if (mysql_num_rows(stiralka_res) > 0) {
			mysql_free_result(stiralka_res);
			mysql_free_result(res);
			puts("stiralka_mysql_close...");
			mysql_close(stiralka_mysql);
			mysql_close(mysql);
			return 1;
		} else {
			mysql_free_result(stiralka_res);
		}
		
		puts("\n <Second user check>");
		//вторая проверка, на интервал в 12 часов
		*buf = '\0';
		sprintf(buf, "SELECT s_id, s_entry_permissions FROM stiralka.op_signups, stiralka.op_users WHERE (unix_timestamp(now()) > s_start*60) AND (unix_timestamp(now()) < s_stop*60 + 43200) AND (s_entry_permissions > 0) AND (s_uid = u_id) AND (u_keyid = %s) ORDER BY s_stop asc",row[0]);
		puts(buf);
		
		error = mysql_query(stiralka_mysql, buf);
		if(error){
			puts("[ERROR] in stiralka_mysql_query");
		}
		stiralka_res = mysql_store_result(stiralka_mysql);
		
		if (mysql_num_rows(stiralka_res) > 0) {
			MYSQL_ROW permissions_row;
			while (permissions_row = mysql_fetch_row(stiralka_res)) {
				*buf = '\0';
				sprintf(buf, "UPDATE op_signups SET s_entry_permissions = (%s-1) WHERE (s_id = %s)",permissions_row[1],permissions_row[0]);
				puts(buf);
				error = mysql_query(stiralka_mysql, buf);
				if(error){
					puts("[ERROR] in stiralka_mysql_query");
				} else {
					mysql_free_result(stiralka_res);
					mysql_free_result(res);
					puts("stiralka_mysql_close...");
					mysql_close(stiralka_mysql);
					mysql_close(mysql);
					return 1;
				}
			}	
		} else {
			mysql_free_result(stiralka_res);
		}
	}
		
		mysql_free_result(res);
		puts("stiralka_mysql_close...");
		mysql_close(mysql);
		mysql_close(stiralka_mysql);
	return 0;
}



int mysqlrequest(char *number, int where) { // where = 0(fitness),1(washing),2(bicycle)
    char *location;
    if (where == 0) location = "fitness";
    else if(where == 1) location = "washing";
    else if(where == 2) location = "bicycle"; 
    else location = "unknown";
    
	puts("mysql_init...");
    
	MYSQL *mysql = mysql_init(NULL);
	if(mysql == NULL){
		puts("[ERROR] in mysql_init");
		return 0;
    }
    puts("mysql_real_connect...");
   
	mysql = mysql_real_connect(mysql, ADDRESS, USER, PASSWORD, BASE, 0, NULL, 0);
    if(mysql == NULL){
		puts("[ERROR] in mysql_real_connect");
		return 0;
    }
	
    char *sql_add = "";
    if(where == 2){
		sql_add = "AND access_bicycle = 1";
    }
    if(where == 1){
		sql_add = "AND (access_laundry = 1 OR full_laundry_access = 1)";
    }
	
    char buf[250];
    *buf = '\0';
    sprintf(buf, "select id,name,card from users where trim(card)='Mifare%s'",number,sql_add);
	
    puts("mysql_query...");
    puts(buf);
	
    int error = mysql_query(mysql, buf);
    if(error){
		puts("[ERROR] in mysql_query");
		return 0;
    }
	
    puts("mysql_store_result...");
	
    MYSQL_RES *res = mysql_store_result(mysql);
    if(res == NULL){
		puts("[ERROR] in mysql_store_result");
		return 0;
    }
	
    MYSQL_ROW row;
    if (row = mysql_fetch_row(res)) {
		unsigned long *lengths;
		lengths = mysql_fetch_lengths(res);
		*buf = '\0';
		sprintf(buf, "INSERT INTO log (uid, time,location) VALUES (%.*s,NOW(),'%s')",(int) lengths[0], row[0] ? row[0] : "NULL", location);
		
		puts(buf);
		error = mysql_query(mysql, buf);
		
		if(error){
			puts("[ERROR] in mysql_query(log)");
		}
		
		mysql_free_result(res);
		puts("mysql_close...");
		mysql_close(mysql);
		return 1;
    } else {
		*buf = '\0';
		sprintf(buf, "INSERT INTO violations_log (card, time,location) VALUES ('Mifare%s',NOW(),'%s')",number, location);
		
		puts(buf);
		error = mysql_query(mysql, buf);
		
		if(error){
			puts("[ERROR] in mysql_query(violations_log)");
		}
		
		mysql_free_result(res);
		puts("mysql_close...");
		mysql_close(mysql);
		return 0;
    }
}

int main(){
//daemonize();
read_config("server.conf");

struct termios options;

/*char* devicenames[] = {"/dev/ttyUSB0","/dev/tts/0","/dev/tts/1"};
int  locations[] = {17,0,1};
int device_count = 1;
*/

//stty -echo -echoe -echok -ixon  -F /dev/tts/1
char* devicenames[] = {"/dev/ttyNSC0","/dev/bus/usb/001/002","/dev/bus/usb/001/003"};
int  locations[] = {0,1,2};
int device_count = 1;


int fd[] = {0,0,0};
int i;
int retval;
int fdmax;
char result[1024];
fd_set rfds;
char buffer[1024] = "";
char buffer2[1024] = "";
int buf_cnt = 0;

FD_ZERO(&rfds);
int j=0;
int k;
int new_string_start = 0;
int set_bits = 4;
    
FILE * logfd;

//perror("");
logfd = fopen("/var/log/test.log","w");
//perror("");
printf("ready to open\n");

for (i=0; i<device_count; i++) {
    
    sprintf(buffer,"stty -echo -echoe -echok -ixon  -F  %s ",devicenames[i]);
    system(buffer);
    
    fd[i] = open(devicenames[i], O_RDWR | O_NOCTTY    );
    printf("%s:%i\n",devicenames[i],fd[i]);
    //perror("fuck\n");
//    ioctl(fd[i], TIOCMSET, &set_bits);
    FD_SET(fd[i], &rfds);
    if (fd[i]>=fdmax)
        fdmax = fd[i];
}
buffer[0] = 0;
char str1[50];
char str2[50];
char str3[50];
char str4[50];
char str5[50];
//fprintf(logfd, "helloworld\n");
int sock_fd;
char recv_buf[255];
char lock_buf[4096];


memset(lock_buf,0,4096);




//while ( (sock_fd = get_socket()) == -1) {
//    sleep(2);
//    fprintf(lologfgfd,"trying to reconnect...\n");
//    fflush(logfd);
//} 
    

FILE* f;
//echo 32 > /sys/class/gpio/export
f = fopen("/sys/class/gpio/export","w");
fprintf(f,"32");
fclose(f);

//echo out > /sys/class/gpio/gpio32/direction
f = fopen("/sys/class/gpio/gpio32/direction","w");
fprintf(f,"out");
fclose(f);

//echo 0 > /sys/class/gpio/gpio32/value
f = fopen("/sys/class/gpio/gpio32/value","w");
fprintf(f,"0");
fclose(f);

//echo 33 > /sys/class/gpio/export
f = fopen("/sys/class/gpio/export","w");
fprintf(f,"33");
fclose(f);

//echo out > /sys/class/gpio/gpio33/direction
f = fopen("/sys/class/gpio/gpio33/direction","w");
fprintf(f,"out");
fclose(f);

//echo 0 > /sys/class/gpio/gpio33/value
f = fopen("/sys/class/gpio/gpio33/value","w");
fprintf(f,"0");
fclose(f);

//echo 34 > /sys/class/gpio/export
f = fopen("/sys/class/gpio/export","w");
fprintf(f,"34");
fclose(f);

//echo out > /sys/class/gpio/gpio34/direction
f = fopen("/sys/class/gpio/gpio34/direction","w");
fprintf(f,"out");
fclose(f);

//echo 0 > /sys/class/gpio/gpio34/value
f = fopen("/sys/class/gpio/gpio34/value","w");
fprintf(f,"0");
fclose(f);


//printf("main cycle\n");

while (1) {
    FD_ZERO(&rfds);
    fdmax = 0;
    for (i=0; i<device_count; i++) {
    FD_SET(fd[i], &rfds);
        if (fd[i]>=fdmax)
        fdmax = fd[i];
    }
    
    retval = select(fdmax+1,&rfds, NULL, NULL, NULL);
    //puts("yeah fuck me");
    if (retval){
        for (i=0; i<device_count; i++) {
            if (FD_ISSET(fd[i],&rfds)) {
                //printf("%s>>\n",devicenames[i]);
                //k = readport(fd[i],result);
		memset(result,0,1024);
                k = read(fd[i],result,1024);
                new_string_start = 0;
		printf("from device %i:\n%s\n",i,result);
		//printf(logfd,"from device %i:\n%s\n",i,result);
		//fflush(logfd);
		char *openbracket = strchr(result,'[');
		char *closebracket = strchr(result,']');
		if(openbracket && closebracket){
		    char number[50];
		    memset(number,0,50);
		    strncpy(number,openbracket+1,closebracket-openbracket-1);
		    printf("number:%s\n",number);
		    int location = 0;
		    if(strstr(result,"1065")){
			location = 1;
		    }else if(strstr(result,"1110")){
			location = 0;
		    }else if(strstr(result,"1025")){
			location = 2;
		    }
		    int z = mysqlrequest(number,location);
			if (location == 1) {
				z = stiralka_time_access_request(number);
			}
			
		    *openbracket = 0;
		    if(strstr(result,"1065") && z){
			printf("1065!\n");
			//echo 1 > /sys/class/gpio/gpio32/value
			f = fopen("/sys/class/gpio/gpio32/value","w");
			fprintf(f,"1");
			fclose(f);

			sleep(2);

			//echo 0 > /sys/class/gpio/gpio32/value
			f = fopen("/sys/class/gpio/gpio32/value","w");
			fprintf(f,"0");
			fclose(f);
		    }
		    if(strstr(result,"1110") && z){
			printf("1110!\n");
			//echo 1 > /sys/class/gpio/gpio33/value
			f = fopen("/sys/class/gpio/gpio33/value","w");
			fprintf(f,"1");
			fclose(f);

			sleep(2);

			//echo 0 > /sys/class/gpio/gpio33/value
			f = fopen("/sys/class/gpio/gpio33/value","w");
			fprintf(f,"0");
			fclose(f);

		    }
		    if(strstr(result,"1025") && z){
			printf("1025!\n");
			//echo 1 > /sys/class/gpio/gpio34/value
			f = fopen("/sys/class/gpio/gpio34/value","w");
			fprintf(f,"1");
			fclose(f);

			sleep(2);

			//echo 0 > /sys/class/gpio/gpio34/value
			f = fopen("/sys/class/gpio/gpio34/value","w");
			fprintf(f,"0");
			fclose(f);

		    }
		}
                fflush(0);
                
                
            }
        }
        
    }
    
    
    
}





    



}


